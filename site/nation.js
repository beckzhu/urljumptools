var nationList = [
    {
        "short": "AD",
        "value": "安道尔共和国",
        "en": "Andorra",
        "tel": "376",
        "pinyin": "adeghg"
    },
    {
        "short": "AE",
        "value": "阿拉伯联合酋长国",
        "en": "UnitedArabEmirates",
        "tel": "971",
        "pinyin": "alblhqzg"
    },
    {
        "short": "AF",
        "value": "阿富汗",
        "en": "Afghanistan",
        "tel": "93",
        "pinyin": "afh"
    },
    {
        "short": "AG",
        "value": "安提瓜和巴布达",
        "en": "AntiguaandBarbuda",
        "tel": "1268",
        "pinyin": "atghbbd"
    },
    {
        "short": "AI",
        "value": "安圭拉岛",
        "en": "Anguilla",
        "tel": "1264",
        "pinyin": "agld"
    },
    {
        "short": "AL",
        "value": "阿尔巴尼亚",
        "en": "Albania",
        "tel": "355",
        "pinyin": "aebny"
    },
    {
        "short": "AM",
        "value": "阿美尼亚",
        "en": "Armenia",
        "tel": "374",
        "pinyin": "amny"
    },
    {
        "short": "",
        "value": "阿森松",
        "en": "Ascension",
        "tel": "247",
        "pinyin": "als"

    },
    {
        "short": "AO",
        "value": "安哥拉",
        "en": "Angola",
        "tel": "244",
        "pinyin": "agl"

    },
    {
        "short": "AR",
        "value": "阿根廷",
        "en": "Argentina",
        "tel": "54",
        "pinyin": "agt"

    },
    {
        "short": "AT",
        "value": "奥地利",
        "en": "Austria",
        "tel": "43",
        "pinyin": "adl"

    },
    {
        "short": "AU",
        "value": "澳大利亚",
        "en": "Australia",
        "tel": "61",
        "pinyin": "adly"

    },
    {
        "short": "AZ",
        "value": "阿塞拜疆",
        "en": "Azerbaijan",
        "tel": "994",
        "pinyin": "asbj"

    },
    {
        "short": "BB",
        "value": "巴巴多斯",
        "en": "Barbados",
        "tel": "1246",
        "pinyin": "bbds"

    },
    {
        "short": "BD",
        "value": "孟加拉国",
        "en": "Bangladesh",
        "tel": "880",
        "pinyin": "mjlg"

    },
    {
        "short": "BE",
        "value": "比利时",
        "en": "Belgium",
        "tel": "32",
        "pinyin": "bls"

    },
    {
        "short": "BF",
        "value": "布基纳法索",
        "en": "Burkina-faso",
        "tel": "226",
        "pinyin": "bjnfs"

    },
    {
        "short": "BG",
        "value": "保加利亚",
        "en": "Bulgaria",
        "tel": "359",
        "pinyin": "bjly"

    },
    {
        "short": "BH",
        "value": "巴林",
        "en": "Bahrain",
        "tel": "973",
        "pinyin": "bl"

    },
    {
        "short": "BI",
        "value": "布隆迪",
        "en": "Burundi",
        "tel": "257",
        "pinyin": "bld"

    },
    {
        "short": "BJ",
        "value": "贝宁",
        "en": "Benin",
        "tel": "229",
        "pinyin": "bl"

    },
    {
        "short": "BL",
        "value": "巴勒斯坦",
        "en": "Palestine",
        "tel": "970",
        "pinyin": "blst"

    },
    {
        "short": "BM",
        "value": "百慕大群岛",
        "en": "BermudaIs.",
        "tel": "1441",
        "pinyin": "bmdqd"

    },
    {
        "short": "BN",
        "value": "文莱",
        "en": "Brunei",
        "tel": "673",
        "pinyin": "wl"

    },
    {
        "short": "BO",
        "value": "玻利维亚",
        "en": "Bolivia",
        "tel": "591",
        "pinyin": "blwy"

    },
    {
        "short": "BR",
        "value": "巴西",
        "en": "Brazil",
        "tel": "55",
        "pinyin": "bx"

    },
    {
        "short": "BS",
        "value": "巴哈马",
        "en": "Bahamas",
        "tel": "1242",
        "pinyin": "bhm"

    },
    {
        "short": "BW",
        "value": "博茨瓦纳",
        "en": "Botswana",
        "tel": "267",
        "pinyin": "bcwn"

    },
    {
        "short": "BY",
        "value": "白俄罗斯",
        "en": "Belarus",
        "tel": "375",
        "pinyin": "bels"

    },
    {
        "short": "BZ",
        "value": "伯利兹",
        "en": "Belize",
        "tel": "501",
        "pinyin": "blz"

    },
    {
        "short": "CA",
        "value": "加拿大",
        "en": "Canada",
        "tel": "1",
        "pinyin": "jnd"

    },
    {
        "short": "",
        "value": "开曼群岛",
        "en": "CaymanIs.",
        "tel": "1345",
        "pinyin": "kmqd"

    },
    {
        "short": "CF",
        "value": "中非共和国",
        "en": "CentralAfricanRepublic",
        "tel": "236",
        "pinyin": "zfghg"

    },
    {
        "short": "CG",
        "value": "刚果",
        "en": "Congo",
        "tel": "242",
        "pinyin": "gg"

    },
    {
        "short": "CH",
        "value": "瑞士",
        "en": "Switzerland",
        "tel": "41",
        "pinyin": "rs"

    },
    {
        "short": "CK",
        "value": "库克群岛",
        "en": "CookIs.",
        "tel": "682",
        "pinyin": "kkqd"

    },
    {
        "short": "CL",
        "value": "智利",
        "en": "Chile",
        "tel": "56",
        "pinyin": "zl"

    },
    {
        "short": "CM",
        "value": "喀麦隆",
        "en": "Cameroon",
        "tel": "237",
        "pinyin": "kml"

    },
    {
        "short": "CN",
        "value": "中国",
        "en": "China",
        "tel": "86",
        "pinyin": "zg"

    },
    {
        "short": "CO",
        "value": "哥伦比亚",
        "en": "Colombia",
        "tel": "57",
        "pinyin": "glby"

    },
    {
        "short": "CR",
        "value": "哥斯达黎加",
        "en": "CostaRica",
        "tel": "506",
        "pinyin": "gsdlj"

    },
    {
        "short": "CS",
        "value": "捷克",
        "en": "Czech",
        "tel": "420",
        "pinyin": "jk"

    },
    {
        "short": "CU",
        "value": "古巴",
        "en": "Cuba",
        "tel": "53",
        "pinyin": "gb"

    },
    {
        "short": "CY",
        "value": "塞浦路斯",
        "en": "Cyprus",
        "tel": "357",
        "pinyin": "spls"

    },
    {
        "short": "CZ",
        "value": "捷克",
        "en": "CzechRepublic",
        "tel": "420",
        "pinyin": "jk"

    },
    {
        "short": "DE",
        "value": "德国",
        "en": "Germany",
        "tel": "49",
        "pinyin": "dg"

    },
    {
        "short": "DJ",
        "value": "吉布提",
        "en": "Djibouti",
        "tel": "253",
        "pinyin": "jbt"

    },
    {
        "short": "DK",
        "value": "丹麦",
        "en": "Denmark",
        "tel": "45",
        "pinyin": "dm"

    },
    {
        "short": "DO",
        "value": "多米尼加共和国",
        "en": "DominicaRep.",
        "tel": "1890",
        "pinyin": "dmnjghg"

    },
    {
        "short": "DZ",
        "value": "阿尔及利亚",
        "en": "Algeria",
        "tel": "213",
        "pinyin": "aejly"

    },
    {
        "short": "EC",
        "value": "厄瓜多尔",
        "en": "Ecuador",
        "tel": "593",
        "pinyin": "egde"

    },
    {
        "short": "EE",
        "value": "爱沙尼亚",
        "en": "Estonia",
        "tel": "372",
        "pinyin": "asny"

    },
    {
        "short": "EG",
        "value": "埃及",
        "en": "Egypt",
        "tel": "20",
        "pinyin": "ej"

    },
    {
        "short": "ES",
        "value": "西班牙",
        "en": "Spain",
        "tel": "34",
        "pinyin": "xby"

    },
    {
        "short": "ET",
        "value": "埃塞俄比亚",
        "en": "Ethiopia",
        "tel": "251",
        "pinyin": "aseby"

    },
    {
        "short": "FI",
        "value": "芬兰",
        "en": "Finland",
        "tel": "358",
        "pinyin": "fl"

    },
    {
        "short": "FJ",
        "value": "斐济",
        "en": "Fiji",
        "tel": "679",
        "pinyin": "fj"

    },
    {
        "short": "FR",
        "value": "法国",
        "en": "France",
        "tel": "33",
        "pinyin": "fg"

    },
    {
        "short": "GA",
        "value": "加蓬",
        "en": "Gabon",
        "tel": "241",
        "pinyin": "jp"

    },
    {
        "short": "GB",
        "value": "英国",
        "en": "UnitedKiongdom",
        "tel": "44",
        "pinyin": "yg"

    },
    {
        "short": "GD",
        "value": "格林纳达",
        "en": "Grenada",
        "tel": "1809",
        "pinyin": "glnd"

    },
    {
        "short": "GE",
        "value": "格鲁吉亚",
        "en": "Georgia",
        "tel": "995",
        "pinyin": "gljy"

    },
    {
        "short": "GF",
        "value": "法属圭亚那",
        "en": "FrenchGuiana",
        "tel": "594",
        "pinyin": "fsgyn"

    },
    {
        "short": "GH",
        "value": "加纳",
        "en": "Ghana",
        "tel": "233",
        "pinyin": "jn"

    },
    {
        "short": "GI",
        "value": "直布罗陀",
        "en": "Gibraltar",
        "tel": "350",
        "pinyin": "zblt"

    },
    {
        "short": "GM",
        "value": "冈比亚",
        "en": "Gambia",
        "tel": "220",
        "pinyin": "gby"

    },
    {
        "short": "GN",
        "value": "几内亚",
        "en": "Guinea",
        "tel": "224",
        "pinyin": "jny"

    },
    {
        "short": "GR",
        "value": "希腊",
        "en": "Greece",
        "tel": "30",
        "pinyin": "xl"

    },
    {
        "short": "GT",
        "value": "危地马拉",
        "en": "Guatemala",
        "tel": "502",
        "pinyin": "wdml"

    },
    {
        "short": "GU",
        "value": "关岛",
        "en": "Guam",
        "tel": "1671",
        "pinyin": "gd"

    },
    {
        "short": "GY",
        "value": "圭亚那",
        "en": "Guyana",
        "tel": "592",
        "pinyin": "gyn"

    },
    {
        "short": "HN",
        "value": "洪都拉斯",
        "en": "Honduras",
        "tel": "504",
        "pinyin": "hdls"

    },
    {
        "short": "HT",
        "value": "海地",
        "en": "Haiti",
        "tel": "509",
        "pinyin": "hd"

    },
    {
        "short": "HU",
        "value": "匈牙利",
        "en": "Hungary",
        "tel": "36",
        "pinyin": "xyl"

    },
    {
        "short": "ID",
        "value": "印度尼西亚",
        "en": "Indonesia",
        "tel": "62",
        "pinyin": "ydnxy"

    },
    {
        "short": "IE",
        "value": "爱尔兰",
        "en": "Ireland",
        "tel": "353",
        "pinyin": "ael"

    },
    {
        "short": "IL",
        "value": "以色列",
        "en": "Israel",
        "tel": "972",
        "pinyin": "ysl"

    },
    {
        "short": "IN",
        "value": "印度",
        "en": "India",
        "tel": "91",
        "pinyin": "yd"

    },
    {
        "short": "IQ",
        "value": "伊拉克",
        "en": "Iraq",
        "tel": "964",
        "pinyin": "ylk"

    },
    {
        "short": "IR",
        "value": "伊朗",
        "en": "Iran",
        "tel": "98",
        "pinyin": "yl"

    },
    {
        "short": "IS",
        "value": "冰岛",
        "en": "Iceland",
        "tel": "354",
        "pinyin": "bd"

    },
    {
        "short": "IT",
        "value": "意大利",
        "en": "Italy",
        "tel": "39",
        "pinyin": "ydl"

    },
    {
        "short": "",
        "value": "科特迪瓦",
        "en": "IvoryCoast",
        "tel": "225",
        "pinyin": "ktdw"

    },
    {
        "short": "JM",
        "value": "牙买加",
        "en": "Jamaica",
        "tel": "1876",
        "pinyin": "ymj"

    },
    {
        "short": "JO",
        "value": "约旦",
        "en": "Jordan",
        "tel": "962",
        "pinyin": "yd"

    },
    {
        "short": "JP",
        "value": "日本",
        "en": "Japan",
        "tel": "81",
        "pinyin": "rb"

    },
    {
        "short": "KE",
        "value": "肯尼亚",
        "en": "Kenya",
        "tel": "254",
        "pinyin": "kny"

    },
    {
        "short": "KG",
        "value": "吉尔吉斯坦",
        "en": "Kyrgyzstan",
        "tel": "331",
        "pinyin": "jejst"

    },
    {
        "short": "KH",
        "value": "柬埔寨",
        "en": "Kampuchea(Cambodia)",
        "tel": "855",
        "pinyin": "jpz"

    },
    {
        "short": "KP",
        "value": "朝鲜",
        "en": "NorthKorea",
        "tel": "850",
        "pinyin": "cx"

    },
    {
        "short": "KR",
        "value": "韩国",
        "en": "Korea",
        "tel": "82",
        "pinyin": "hg"


    },
    {
        "short": "KT",
        "value": "科特迪瓦共和国",
        "en": "RepublicofIvoryCoast",
        "tel": "225",
        "pinyin": "ktdwghg"

    },
    {
        "short": "KW",
        "value": "科威特",
        "en": "Kuwait",
        "tel": "965",
        "pinyin": "kwt"

    },
    {
        "short": "KZ",
        "value": "哈萨克斯坦",
        "en": "Kazakstan",
        "tel": "327",
        "pinyin": "hskst"

    },
    {
        "short": "LA",
        "value": "老挝",
        "en": "Laos",
        "tel": "856",
        "pinyin": "lw"

    },
    {
        "short": "LB",
        "value": "黎巴嫩",
        "en": "Lebanon",
        "tel": "961",
        "pinyin": "lbn"

    },
    {
        "short": "LC",
        "value": "圣卢西亚",
        "en": "St.Lucia",
        "tel": "1758",
        "pinyin": "slxy"

    },
    {
        "short": "LI",
        "value": "列支敦士登",
        "en": "Liechtenstein",
        "tel": "423",
        "pinyin": "lzdsd"

    },
    {
        "short": "LK",
        "value": "斯里兰卡",
        "en": "SriLanka",
        "tel": "94",
        "pinyin": "sllk"

    },
    {
        "short": "LR",
        "value": "利比里亚",
        "en": "Liberia",
        "tel": "231",
        "pinyin": "lbly"

    },
    {
        "short": "LS",
        "value": "莱索托",
        "en": "Lesotho",
        "tel": "266",
        "pinyin": "lst"

    },
    {
        "short": "LT",
        "value": "立陶宛",
        "en": "Lithuania",
        "tel": "370",
        "pinyin": "ltw"

    },
    {
        "short": "LU",
        "value": "卢森堡",
        "en": "Luxembourg",
        "tel": "352",
        "pinyin": "lsb"

    },
    {
        "short": "LV",
        "value": "拉脱维亚",
        "en": "Latvia",
        "tel": "371",
        "pinyin": "ltwy"

    },
    {
        "short": "LY",
        "value": "利比亚",
        "en": "Libya",
        "tel": "218",
        "pinyin": "lby"

    },
    {
        "short": "MA",
        "value": "摩洛哥",
        "en": "Morocco",
        "tel": "212",
        "pinyin": "mlg"

    },
    {
        "short": "MC",
        "value": "摩纳哥",
        "en": "Monaco",
        "tel": "377",
        "pinyin": "mng"

    },
    {
        "short": "MD",
        "value": "摩尔多瓦",
        "en": "Moldova,Republicof",
        "tel": "373",
        "pinyin": "medw"

    },
    {
        "short": "MG",
        "value": "马达加斯加",
        "en": "Madagascar",
        "tel": "261",
        "pinyin": "mdjsj"

    },
    {
        "short": "ML",
        "value": "马里",
        "en": "Mali",
        "tel": "223",
        "pinyin": "ml"

    },
    {
        "short": "MM",
        "value": "缅甸",
        "en": "Burma",
        "tel": "95",
        "pinyin": "md"

    },
    {
        "short": "MN",
        "value": "蒙古",
        "en": "Mongolia",
        "tel": "976",
        "pinyin": "mg"

    },
    {
        "short": "MS",
        "value": "蒙特塞拉特岛",
        "en": "MontserratIs",
        "tel": "1664",
        "pinyin": "mtsstd"

    },
    {
        "short": "MT",
        "value": "马耳他",
        "en": "Malta",
        "tel": "356",
        "pinyin": "met"

    },
    {
        "short": "",
        "value": "马里亚那群岛",
        "en": "MarianaIs",
        "tel": "1670",
        "pinyin": "mlynqd"

    },
    {
        "short": "",
        "value": "马提尼克",
        "en": "Martinique",
        "tel": "596",
        "pinyin": "mtnk"

    },
    {
        "short": "MU",
        "value": "毛里求斯",
        "en": "Mauritius",
        "tel": "230",
        "pinyin": "mlqs"

    },
    {
        "short": "MV",
        "value": "马尔代夫",
        "en": "Maldives",
        "tel": "960",
        "pinyin": "medf"


    },
    {
        "short": "MW",
        "value": "马拉维",
        "en": "Malawi",
        "tel": "265",
        "pinyin": "mlw"

    },
    {
        "short": "MX",
        "value": "墨西哥",
        "en": "Mexico",
        "tel": "52",
        "pinyin": "mxg"

    },
    {
        "short": "MY",
        "value": "马来西亚",
        "en": "Malaysia",
        "tel": "60",
        "pinyin": "mlxy"

    },
    {
        "short": "MZ",
        "value": "莫桑比克",
        "en": "Mozambique",
        "tel": "258",
        "pinyin": "msbk"

    },
    {
        "short": "NA",
        "value": "纳米比亚",
        "en": "Namibia",
        "tel": "264",
        "pinyin": "nmby"

    },
    {
        "short": "NE",
        "value": "尼日尔",
        "en": "Niger",
        "tel": "977",
        "pinyin": "nre"

    },
    {
        "short": "NG",
        "value": "尼日利亚",
        "en": "Nigeria",
        "tel": "234",
        "pinyin": "nrly"

    },
    {
        "short": "NI",
        "value": "尼加拉瓜",
        "en": "Nicaragua",
        "tel": "505",
        "pinyin": "njlg"

    },
    {
        "short": "NL",
        "value": "荷兰",
        "en": "Netherlands",
        "tel": "31",
        "pinyin": "hl"

    },
    {
        "short": "NO",
        "value": "挪威",
        "en": "Norway",
        "tel": "47",
        "pinyin": "nw"

    },
    {
        "short": "NP",
        "value": "尼泊尔",
        "en": "Nepal",
        "tel": "977",
        "pinyin": "nbe"

    },
    {
        "short": "",
        "value": "荷属安的列斯",
        "en": "NetheriandsAntilles",
        "tel": "599",
        "pinyin": "hsadls"

    },
    {
        "short": "NR",
        "value": "瑙鲁",
        "en": "Nauru",
        "tel": "674",
        "pinyin": "nl"

    },
    {
        "short": "NZ",
        "value": "新西兰",
        "en": "NewZealand",
        "tel": "64",
        "pinyin": "xxl"

    },
    {
        "short": "OM",
        "value": "阿曼",
        "en": "Oman",
        "tel": "968",
        "pinyin": "am"

    },
    {
        "short": "PA",
        "value": "巴拿马",
        "en": "Panama",
        "tel": "507",
        "pinyin": "bnm"

    },
    {
        "short": "PE",
        "value": "秘鲁",
        "en": "Peru",
        "tel": "51",
        "pinyin": "bl"

    },
    {
        "short": "PF",
        "value": "法属玻利尼西亚",
        "en": "FrenchPolynesia",
        "tel": "689",
        "pinyin": "fsblnxy"

    },
    {
        "short": "PG",
        "value": "巴布亚新几内亚",
        "en": "PapuaNewCuinea",
        "tel": "675",
        "pinyin": "bbyxjny"

    },
    {
        "short": "PH",
        "value": "菲律宾",
        "en": "Philippines",
        "tel": "63",
        "pinyin": "flb"

    },
    {
        "short": "PK",
        "value": "巴基斯坦",
        "en": "Pakistan",
        "tel": "92",
        "pinyin": "bjst"

    },
    {
        "short": "PL",
        "value": "波兰",
        "en": "Poland",
        "tel": "48",
        "pinyin": "bl"

    },
    {
        "short": "PR",
        "value": "波多黎各",
        "en": "PuertoRico",
        "tel": "1787",
        "pinyin": "bdlg"

    },
    {
        "short": "PT",
        "value": "葡萄牙",
        "en": "Portugal",
        "tel": "351",
        "pinyin": "pty"

    },
    {
        "short": "PY",
        "value": "巴拉圭",
        "en": "Paraguay",
        "tel": "595",
        "pinyin": "blg"

    },
    {
        "short": "QA",
        "value": "卡塔尔",
        "en": "Qatar",
        "tel": "974",
        "pinyin": "kte"

    },
    {
        "short": "",
        "value": "留尼旺",
        "en": "Reunion",
        "tel": "262",
        "pinyin": "lnw"

    },
    {
        "short": "RO",
        "value": "罗马尼亚",
        "en": "Romania",
        "tel": "40",
        "pinyin": "lmny"

    },
    {
        "short": "RU",
        "value": "俄罗斯",
        "en": "Russia",
        "tel": "7",
        "pinyin": "els"

    },
    {
        "short": "SA",
        "value": "沙特阿拉伯",
        "en": "SaudiArabia",
        "tel": "966",
        "pinyin": "stalb"

    },
    {
        "short": "SB",
        "value": "所罗门群岛",
        "en": "SolomonIs",
        "tel": "677",
        "pinyin": "slmqd"

    },
    {
        "short": "SC",
        "value": "塞舌尔",
        "en": "Seychelles",
        "tel": "248",
        "pinyin": "sse"

    },
    {
        "short": "SD",
        "value": "苏丹",
        "en": "Sudan",
        "tel": "249",
        "pinyin": "sd"

    },
    {
        "short": "SE",
        "value": "瑞典",
        "en": "Sweden",
        "tel": "46",
        "pinyin": "rd"

    },
    {
        "short": "SG",
        "value": "新加坡",
        "en": "Singapore",
        "tel": "65",
        "pinyin": "xjp"

    },
    {
        "short": "SI",
        "value": "斯洛文尼亚",
        "en": "Slovenia",
        "tel": "386",
        "pinyin": "slwny"

    },
    {
        "short": "SK",
        "value": "斯洛伐克",
        "en": "Slovakia",
        "tel": "421",
        "pinyin": "slfk"

    },
    {
        "short": "SL",
        "value": "塞拉利昂",
        "en": "SierraLeone",
        "tel": "232",
        "pinyin": "slla"

    },
    {
        "short": "SM",
        "value": "圣马力诺",
        "en": "SanMarino",
        "tel": "378",
        "pinyin": "smln"

    },
    {
        "short": "",
        "value": "西萨摩亚",
        "en": "SanMarino",
        "tel": "685",
        "pinyin": "xsmy"

    },
    {
        "short": "SN",
        "value": "塞内加尔",
        "en": "Senegal",
        "tel": "221",
        "pinyin": "snje"

    },
    {
        "short": "SO",
        "value": "索马里",
        "en": "Somali",
        "tel": "252",
        "pinyin": "sml"

    },
    {
        "short": "SR",
        "value": "苏里南",
        "en": "Surivalue",
        "tel": "597",
        "pinyin": "sln"

    },
    {
        "short": "ST",
        "value": "圣多美和普林西比",
        "en": "SaoTomeandPrincipe",
        "tel": "239",
        "pinyin": "sdmhplxb"

    },
    {
        "short": "SV",
        "value": "萨尔瓦多",
        "en": "EISalvador",
        "tel": "503",
        "pinyin": "sewd"

    },
    {
        "short": "SY",
        "value": "叙利亚",
        "en": "Syria",
        "tel": "963",
        "pinyin": "xly"

    },
    {
        "short": "SZ",
        "value": "斯威士兰",
        "en": "Swaziland",
        "tel": "268",
        "pinyin": "swsl"

    },
    {
        "short": "TD",
        "value": "乍得",
        "en": "Chad",
        "tel": "235",
        "pinyin": "zd"

    },
    {
        "short": "TG",
        "value": "多哥",
        "en": "Togo",
        "tel": "228",
        "pinyin": "dg"

    },
    {
        "short": "TH",
        "value": "泰国",
        "en": "Thailand",
        "tel": "66",
        "pinyin": "tg"

    },
    {
        "short": "TJ",
        "value": "塔吉克斯坦",
        "en": "Tajikstan",
        "tel": "992",
        "pinyin": "tjkst"

    },
    {
        "short": "TM",
        "value": "土库曼斯坦",
        "en": "Turkmenistan",
        "tel": "993",
        "pinyin": "tkmst"

    },
    {
        "short": "TN",
        "value": "突尼斯",
        "en": "Tunisia",
        "tel": "216",
        "pinyin": "tns"

    },
    {
        "short": "TO",
        "value": "汤加",
        "en": "Tonga",
        "tel": "676",
        "pinyin": "tj"

    },
    {
        "short": "TR",
        "value": "土耳其",
        "en": "Turkey",
        "tel": "90",
        "pinyin": "teq"

    },
    {
        "short": "TT",
        "value": "特立尼达和多巴哥",
        "en": "TrinidadandTobago",
        "tel": "1809",
        "pinyin": "tlndhdbg"

    },
    {
        "short": "TZ",
        "value": "坦桑尼亚",
        "en": "Tanzania",
        "tel": "255",
        "pinyin": "tsny"

    },
    {
        "short": "UA",
        "value": "乌克兰",
        "en": "Ukraine",
        "tel": "380",
        "pinyin": "wkl"

    },
    {
        "short": "UG",
        "value": "乌干达",
        "en": "Uganda",
        "tel": "256",
        "pinyin": "wgd"

    },
    {
        "short": "US",
        "value": "美国",
        "en": "UnitedStatesofAmerica",
        "tel": "1",
        "pinyin": "mg"

    },
    {
        "short": "UY",
        "value": "乌拉圭",
        "en": "Uruguay",
        "tel": "598",
        "pinyin": "wlg"

    },
    {
        "short": "UZ",
        "value": "乌兹别克斯坦",
        "en": "Uzbekistan",
        "tel": "233",
        "pinyin": "wzbkst"

    },
    {
        "short": "VC",
        "value": "圣文森特岛",
        "en": "SaintVincent",
        "tel": "1784",
        "pinyin": "swstd"

    },
    {
        "short": "VE",
        "value": "委内瑞拉",
        "en": "Venezuela",
        "tel": "58",
        "pinyin": "wnrl"

    },
    {
        "short": "VN",
        "value": "越南",
        "en": "Vietnam",
        "tel": "84",
        "pinyin": "yn"

    },
    {
        "short": "YE",
        "value": "也门",
        "en": "Yemen",
        "tel": "967",
        "pinyin": "ym"

    },
    {
        "short": "YU",
        "value": "南斯拉夫",
        "en": "Yugoslavia",
        "tel": "381",
        "pinyin": "nslf"

    },
    {
        "short": "ZA",
        "value": "南非",
        "en": "SouthAfrica",
        "tel": "27",
        "pinyin": "nf"

    },
    {
        "short": "ZM",
        "value": "赞比亚",
        "en": "Zambia",
        "tel": "260",
        "pinyin": "zby"

    },
    {
        "short": "ZR",
        "value": "扎伊尔",
        "en": "Zaire",
        "tel": "243",
        "pinyin": "zye"

    },
    {
        "short": "ZW",
        "value": "津巴布韦",
        "en": "Zimbabwe",
        "tel": "263",
        "pinyin": "jbbw"

    }
];