<?php
session_start();
//设置错误处理回调
function Admin_ErrorHandler($errcode, $errstr, $errfile, $errline)
{
    file_put_contents('data/admin.log', $errstr."\n", FILE_APPEND);
    throw new Exception($errstr);
}
set_error_handler("Admin_ErrorHandler");

//跳转到文件管理器
if (array_key_exists("p", $_GET)) {
    if (array_key_exists('isLogin', $_SESSION)) {
        define('FM_EMBED', true);
        define('FM_SELF_URL', $_SERVER['PHP_SELF']);
        require 'FileBrowser.php';
        return;
    }
}
if (array_key_exists("type", $_GET)) {
    $type = $_GET["type"];
    $resobj = array('success' => true, 'message' => '', 'value');
    try {
        switch ($type) {
            case 'login':
                LoginAccount();
                break;
            case 'saveConfig':
                SaveConfig();
                break;
            case 'modifyPassword':
                ModifyPassword();
                break;
            case 'queryLoadUrl':
                $resobj['value'] = QueryLoadUrl();
                break;
        }
    } catch (Throwable $e) {
        $resobj['success'] = false;
        $resobj['message'] = $e->getMessage();
    }
    echo json_encode($resobj);
    return;
}
//登入账号
function LoginAccount()
{
    if (!array_key_exists("password", $_POST)) throw new Exception('参数错误');
    if (!file_exists("data/certified") || filesize("data/certified") < 1) {
        $_SESSION['isLogin'] = true;
        return;
    }
    $contents = file_get_contents("data/certified");

    if ($_POST["password"] == $contents) {
        $_SESSION['isLogin'] = true;
    } else {
        throw new Exception('密码错误');
    }
}

//修改密码
function ModifyPassword()
{
    if (array_key_exists("password", $_POST)) {
        if (!array_key_exists("isLogin", $_SESSION)) {
            $retArray['message'] = '当前登入状态已过期,请刷新后重新登入';
        } else {
            if (!is_dir('data')) mkdir('data');
            file_put_contents('data/certified', $_POST["password"]);
        }
    } else {
        throw new Exception('参数错误');
    }
}

//保存配置
function SaveConfig()
{
    if (array_key_exists('data', $_POST)) {
        if (!array_key_exists("isLogin", $_SESSION)) {
            $retArray['message'] = '当前登入状态已过期,请刷新后重新登入';
        } else {
            if (!is_dir('data')) mkdir('data');
            file_put_contents('data/settings.json', $_POST['data']);
        }
    } else {
        throw new Exception('参数错误');
    }
}
//取文件后缀名  小写
function getExtName($filename)
{
    $arr = explode('.', $filename);
    return strtolower(array_pop($arr));;
}
//查询本地网页
function QueryLoadUrl()
{
    $list = array();

    function filterfiles($file)
    {
        $extNames = array('php', 'htm', 'html');
        $exclude = array('./Admin.php', './FileBrowser.php', './index.php', './utility.php');
        $extname = getExtName($file);
        return in_array($extname, $extNames, false) && !in_array($file, $exclude, false);
    }
    $files = scandir('./');
    foreach ($files as $m) {
        $filename = './' . $m;
        if ($m != '.' && $m != '..' && is_dir($m)) {
            $dirfiles = scandir($filename);
            foreach ($dirfiles as $n) {
                $chlieFile = $filename . '/' . $n;
                if (filterfiles($n)) {
                    array_push($list, array('value' => $chlieFile));
                }
            }
        } else if (filterfiles($filename)) {
            array_push($list, array('value' => $filename));
        }
    }
    return  $list;
}

?>
    <?php
    //未登入状态
    if (!array_key_exists('isLogin', $_SESSION)) {
        ?>
        <!DOCTYPE html>
        <html>

        <head>
            <meta charset="UTF-8">
            <link rel="stylesheet" href="./site/element.css">
            <script src="./site/vue.js"></script>
            <script src="./site/element.js"></script>
            <script src="./site/jquery.min.js"></script>
            <style>
                html,
                body {
                    display: flex;
                    align-items: center;
                    justify-content: center;
                    width: 100%;
                    height: 100%;
                    margin: 0;
                    padding: 0;
                }

                [v-cloak] {
                    display: none !important;
                }
            </style>
        </head>

        <body>
            <div id="app" v-cloak>
                <div style="width: 400px;">
                    <el-input style="width: 280px;" placeholder="请输入密码" v-model="password" @keyup.enter.native="loginForm" show-password></el-input>
                    <el-button style="width: 80px;" @click="LoginAccount">登入</el-button>
                </div>
            </div>
        </body>

        <script>
            var Main = {
                data() {
                    return {
                        password: ''
                    };
                },
                methods: {
                    LoginAccount() {
                        var vue = this;
                        $.post("?type=login", {
                            password: this.password
                        }, function(data) {
                            result = JSON.parse(data)
                            if (result.success) {
                                location.reload();
                            } else {
                                vue.$alert(result.message, '错误', {
                                    confirmButtonText: '确定',
                                });
                            }
                        });
                    },
                },
            }
            var Ctor = Vue.extend(Main);
            var vue = new Ctor().$mount('#app');
        </script>

        </html>
    <?php
    }
    //登入状态
    else {
        function LoadConfig()
        {
            if (file_exists("data/settings.json")) {
                return file_get_contents("data/settings.json");
            } else {
                $resobj = array(
                    'nationurl' => array(array('name' => '', 'url' => '', 'key' => '1576159617')),
                    'priorUrl' => '',
                    'afterUrl' => '',
                    'accessCount' => 0,
                    'jumpMode' => 'A',
                    'enablejumpMode' => 2,
                    'definedUrl' => array(array('name' => '', 'url' => '', 'key' => '1576159617')),
                    'ipInterAddr' => '',
                );
                return json_encode($resobj);
            }
        }

        //获取当前访问量
        function GetAccessCount()
        {
            $contents = '0';
            if (file_exists("data/count")) {
                $contents = file_get_contents("data/count");
            }
            return (int) $contents;
        }

        //取当前index.php地址
        function GetCurrentIndexUrl()
        {
            $url = 'http://';
            if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') {
                $url = 'https://';
            }
            // 判断端口
            if ($_SERVER['SERVER_PORT'] != '80') {
                $url .= $_SERVER['SERVER_NAME'] . ':' . $_SERVER['SERVER_PORT'] . ':';
            } else {
                $url .= $_SERVER['SERVER_NAME'];
            }
            echo $url . '/index.php';
        }

        $settingsText = LoadConfig();
        $accessCount = GetAccessCount()
        ?>
        <!DOCTYPE html>
        <html>

        <head>
            <meta charset="UTF-8">
            <link rel="stylesheet" href="./site/element.css">
            <script src="./site/vue.js"></script>
            <script src="./site/element.js"></script>
            <script src="./site/jquery.min.js"></script>
            <script src="./site/nation.js"></script>

            <style>
                html,
                body {
                    width: 100%;
                    height: 100%;
                    margin: 0;
                    padding: 0;
                    display: flex;
                    justify-content: center;
                }

                .el-tabs--border-card {
                    border: 0px;
                    box-shadow: 0 0 0 0;
                }

                .el-tabs--border-card>.el-tabs__content {
                    padding: 40px;
                }

                .el-collapse-item__header {
                    font-size: 14px;
                    color: #606266;
                }

                .el-tag {
                    font-size: 16px;
                }

                .tabPaneborder {
                    border: 1px solid #DCDFE6;
                    box-shadow: 0 2px 4px 0 rgba(0, 0, 0, .12), 0 0 6px 0 rgba(0, 0, 0, .04);
                    width: 800px;
                    margin-top: 100px;
                    margin-bottom: 100px;
                }

                [v-cloak] {
                    display: none !important;
                }
            </style>

        </head>

        <body>
            <div id="app" v-cloak>
                <el-form class="tabPaneborder" label-width="100px">
                    <el-tabs type="border-card">
                        <el-tab-pane label="策略设置">
                            <el-collapse style="margin-bottom: 12px;">
                                <el-collapse-item title="查看当前跳转状态" name="1" style="padding-left: 10px;">
                                    <el-row style="margin-bottom: 6px;">
                                        <el-col :span="18">
                                            <el-tag>
                                                当前应用链接地址：<?php GetCurrentIndexUrl() ?>
                                            </el-tag>
                                        </el-col>
                                        <el-col :span="6">
                                            <el-button style="float:right;" @click="openCurrentIndex()">打开链接</el-button>
                                        </el-col>
                                    </el-row>
                                    <?php
                                        //读取配置文件
                                        $settings = json_decode($settingsText);
                                        if (!empty($settings->afterUrl) || !empty($settings->priorUrl)) {
                                            include 'utility.php';
                                            $currentJumpText = '';
                                            $currentChientHost = '';
                                            try {
                                                $ip = GetClientAddr();
                                                $currentChientHost = $ip;
                                                $nationur = GetCurrentChileNationur($settings, $ip);
                                                $currentChientHost = $ip . '(' . $nationur . ')';
                                                $currentJumpText = GetJumpUrl($settings, $accessCount, $nationur);
                                                $ationurlJumpList = GetRandomNationurlJump($settings, $accessCount);
                                            } catch (\Throwable $e) {
                                                $currentJumpText = $e->getMessage();
                                                $ationurlJumpList = array();
                                            }

                                            ?>
                                        <el-row style="margin-bottom: 6px;">
                                            <el-col :span="24">
                                                <el-tag>
                                                    当前跳转链接地址：<?php echo $currentJumpText ?>
                                                </el-tag>
                                                <el-tag type="success">
                                                    <?php echo $currentChientHost ?>
                                                </el-tag>
                                            </el-col>
                                        </el-row>
                                        <?php
                                                foreach ($ationurlJumpList as $key => $value) {
                                                    ?>
                                            <el-row style="margin-bottom: 6px;">
                                                <el-col :span="24">
                                                    <el-tag>
                                                        <?php echo $value . ' ：' . $key ?>
                                                    </el-tag>
                                                </el-col>
                                            </el-row>
                                        <?php } ?>
                                    <?php } ?>
                                </el-collapse-item>
                            </el-collapse>

                            <el-form-item label="启用策略前跳转" label-width="120px">
                                <el-autocomplete style="width: 100%;" v-model="priorUrl" :fetch-suggestions="queryDefinedUrl">
                                </el-autocomplete>
                            </el-form-item>

                            <el-form-item label="启用策略后跳转" label-width="120px">
                                <el-autocomplete style="width: 100%;" v-model="afterUrl" :fetch-suggestions="queryDefinedUrl">
                                </el-autocomplete>
                            </el-form-item>

                            <el-form-item v-for="(item, index) in nationurl" label-width="0px" :key="item.key">
                                <el-autocomplete style="width: 120px;" v-model="item.name" :fetch-suggestions="queryNationurl" placeholder="国家"></el-autocomplete>
                                <el-autocomplete style="width: calc(100% - 125px);" placeholder="请输入要跳转的链接" v-model="item.url" :fetch-suggestions="queryDefinedUrl">
                                    <el-button slot="append" @click.prevent="removeNation(item)">删除</el-button>
                                </el-autocomplete>
                            </el-form-item>

                            <el-form-item>
                                <el-button style="float:right;" @click="addNation()">新增国家</el-button>
                            </el-form-item>

                            <el-row>
                                <el-col :span="12">
                                    <el-form-item label="访问计数跳转" label-width="120px">
                                        <el-input type="age" v-model.number="accessCount" autocomplete="off"></el-input>
                                    </el-form-item>
                                </el-col>
                                <el-col :span="12">
                                    <el-form-item label="跳转方式" label-width="120px">
                                        <el-select style="display:inline;" v-model="jumpMode" placeholder="请选择跳转方式">
                                            <el-option label="重定向" value="A"></el-option>
                                            <el-option label="js跳转" value="B"></el-option>
                                            <el-option label="嵌入网页" value="C"></el-option>
                                        </el-select>
                                    </el-form-item>
                                </el-col>
                            </el-row>

                            <el-form-item style="margin-inline: 50px;">
                                <el-radio-group v-model="enablejumpMode">
                                    <el-radio :label="1">启用跳转策略</el-radio>
                                    <el-radio :label="2">启用计数跳转</el-radio>
                                </el-radio-group>
                                <el-tag style="float:right;font-size: 16px">
                                    <?php echo '当前访问量:' . $accessCount ?>
                                </el-tag>
                            </el-form-item>
                        </el-tab-pane>
                        <el-tab-pane label="配置管理">
                            <el-form-item label="IP识别接口url" label-width="120px">
                                <el-input v-model.number="ipInterAddr" autocomplete="off"></el-input>
                            </el-form-item>

                            <el-form-item v-for="(item, index) in definedUrl" label-width="0px" :key="item.key">
                                <el-input style="margin-left: 20px;width: 200px;" v-model="item.name" placeholder="名称">
                                </el-input>
                                <el-autocomplete style="width: calc(100% - 225px);" placeholder="预定义链接" v-model="item.url" :fetch-suggestions="queryLocalUrl">
                                    <el-button slot="append" @click.prevent="removedefinedUrl(item)">删除</el-button>
                                </el-autocomplete>
                            </el-form-item>

                            <el-form-item>
                                <el-button style="float:right;" @click="adddefinedUrl">新增链接</el-button>
                            </el-form-item>
                        </el-tab-pane>
                    </el-tabs>

                    <el-form-item>
                        <el-button style="float:right;margin-right:20px" type="primary" @click="saveConfig()">保存配置</el-button>
                        <el-button style="float:right;margin-right:20px" @click="openFileBrowser">文件管理</el-button>
                        <el-button style="float:right;margin-right:20px" @click="modifyPassword()">修改密码</el-button>
                    </el-form-item>
                </el-form>
            </div>
        </body>

        <script>
            function isRepeat(arr) {
                var hash = {};
                for (var i in arr) {
                    if (hash[arr[i]]) {
                        return true;
                    }
                    hash[arr[i]] = true;
                }
                return false;
            }

            function isEmpty(obj) {
                if (typeof obj == "undefined" || obj == null || obj == "") {
                    return true;
                } else {
                    return false;
                }
            }

            var Main = {
                data() {
                    var config = <?php echo " '$settingsText'" ?>;
                    var redirect = JSON.parse(config);
                    return redirect;
                },
                methods: {
                    queryNationurl(queryString, cb) {
                        var results = queryString ? nationList.filter(m => {
                            return (m.value.indexOf(queryString) >= 0)
                        }) : nationList;
                        cb(results);
                    },
                    queryDefinedUrl(queryString, cb) {
                        cb(this._data.definedUrl.filter(m => {
                            return !isEmpty(m.name)
                        }).map(x => {
                            return {
                                value: x.name + ' (' + x.url + ')'
                            }
                        }));
                    },
                    queryLocalUrl(queryString, cb) {
                        $.get("?type=queryLoadUrl", function(data) {
                            result = JSON.parse(data)
                            if (result.success) {
                                cb(result.value);
                            } else {
                                vue.$alert(result.message, '错误', {
                                    confirmButtonText: '确定',
                                });
                            }
                        });
                    },
                    openCurrentIndex() {
                        window.open("index.php");
                    },
                    saveConfig(formName) {
                        var names = this.nationurl.map(x => {
                            return x.name
                        }).filter(x => {
                            return !isEmpty(x)
                        });
                        if (isRepeat(names)) {
                            this.$alert('国家不能重复', '错误', {
                                confirmButtonText: '确定',
                            });
                            return;
                        }
                        var names = this.definedUrl.map(x => {
                            return x.name
                        }).filter(x => {
                            return !isEmpty(x)
                        });
                        if (isRepeat(names)) {
                            this.$alert('预定义的链接名称不能重复', '错误', {
                                confirmButtonText: '确定',
                            });
                            return;
                        }

                        var vue = this;
                        $.post("?type=saveConfig", {
                            data: JSON.stringify(vue._data)
                        }, function(data) {
                            result = JSON.parse(data);
                            if (result.success) {
                                vue.$alert('保存成功', '信息', {
                                    confirmButtonText: '确定',
                                    callback: action => {
                                        location.reload();
                                    }
                                });
                            } else {
                                vue.$alert(result.message, '错误', {
                                    confirmButtonText: '确定',
                                });
                            }
                        });
                    },
                    modifyPassword(formName) {
                        this.$prompt('请输入新密码', '提示', {
                            confirmButtonText: '确定'
                        }).then(({
                            value
                        }) => {
                            var vue = this;
                            $.post("?type=modifyPassword", {
                                password: value
                            }, function(data) {
                                result = JSON.parse(data)
                                if (result.success) {
                                    vue.$alert('修改密码成功', '信息', {
                                        confirmButtonText: '确定',
                                    });
                                } else {
                                    vue.$alert(result.message, '错误', {
                                        confirmButtonText: '确定',
                                    });
                                }
                            });
                        });
                    },
                    removeNation(item) {
                        var index = this.nationurl.indexOf(item)
                        if (index !== -1) {
                            this.nationurl.splice(index, 1)
                        }
                    },
                    addNation() {
                        this.nationurl.push({
                            name: '',
                            url: this.nationurl[this.nationurl.length - 1].url,
                            key: Date.now()
                        });
                    },
                    removedefinedUrl(item) {
                        var index = this.definedUrl.indexOf(item)
                        if (index !== -1) {
                            this.definedUrl.splice(index, 1)
                        }
                    },
                    adddefinedUrl() {
                        this.definedUrl.push({
                            name: '',
                            url: '',
                            key: Date.now()
                        });
                    },
                    openFileBrowser() {
                        window.open("?p=");
                    }
                },
            }
            var Ctor = Vue.extend(Main);
            var vue = new Ctor().$mount('#app');
        </script>

        </html>

    <?php
    }
    ?>