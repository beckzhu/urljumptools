<?php
//设置错误处理回调
function Index_ErrorHandler($errcode, $errstr, $errfile, $errline)
{
   throw new Exception($errstr);
}
set_error_handler('Index_ErrorHandler');

include 'utility.php';
if (!is_dir('data')) mkdir('data');

//获取当前访问的量  并增加计数
function GetAccessConut()
{
   $num = 0;
   if (file_exists("data/count") && filesize("data/count") > 0) {
      $contents = file_get_contents("data/count");
      $num = (int) $contents;
   }
   $num++;
   file_put_contents("data/count", $num);
   return $num;
}

if (file_exists('data/settings.json')) {
   try {
      //获取和记录 当前页面的访问量
      $accessConut = GetAccessConut();
      //读取配置文件
      $contents = file_get_contents("data/settings.json");
      $settings = json_decode($contents);
      $url = GetJumpUrl($settings, $accessConut);
      JumpUrl($settings->jumpMode, $url);
   } catch (\Throwable $e) {
      http_response_code(404);
      file_put_contents('data/index.log', $e->getMessage() . "\n", FILE_APPEND);
   }
} else {
   file_put_contents('data/index.log', "没有配置要跳转的链接\n", FILE_APPEND);
}

//跳转指定的url
function JumpUrl($type, $url)
{
   switch ($type) {
      case 'A': //301重定向
         header("Location: $url");
         break;
      case 'B': //js脚本跳转
         echo "<!DOCTYPE html>";
         echo "<html lang=\"en\">";
         echo "<head><meta charset=\"UTF-8\"></head>";
         echo "<body><script type=\"text/javascript\">window.location.href = '$url'</script></body>";
         echo "</html>";
         break;
      case 'C':
         readfile($url); //嵌入到当前页面
         break;
      default:
         header(sprintf("Location: %s", $url));
         break;
   }
}

