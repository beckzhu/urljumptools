<?php
require 'IpLocation.php';
use itbdw\Ip\IpLocation;

//设置错误处理回调
function myErrorHandler($errcode,$errstr,$errfile,$errline){
    throw new Exception($errstr);
}
set_error_handler("myErrorHandler");

try {
    $retArray=array('success'=>false,'message'=>'','value'=>'');
    if (array_key_exists("ip", $_GET)){
        $ip=$_GET['ip'];
        $location=IpLocation::getLocation($ip);
        $retArray['value']=json_encode($location);
        $retArray['success']=true;
        $retArray['message']='读取成功';
    }else
    {
        $retArray['message']='参数错误';
    }
} catch (Throwable $e) {
    $retArray['message']=$e->getMessage();
}
echo json_encode($retArray);
?>