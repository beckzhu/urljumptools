<?php

//设置错误处理回调
function Utility_ErrorHandler($errcode, $errstr, $errfile, $errline)
{
   throw new Exception($errstr);
}
set_error_handler('Utility_ErrorHandler');

//取文本中间部分   如果取不到 则返回原来的文本
function GetStrMiddle($str, $strleft, $strright)
{
   $leftPos = strpos($str, $strleft);
   $rightPos = strpos($str, $strright);
   if ($leftPos != false && $rightPos != false) {
      $str = substr($str, $leftPos + 1, $rightPos - $leftPos - 1);
   }
   return $str;
}

//完善url  没有http的  加http
function PerfectUrl($url)
{
   //url + http://
   if (empty($url) || strlen($url) < 4) throw new Exception('没有配置要跳转的链接');
   if (strnatcasecmp(substr($url, 0, 4), 'http') != 0 && strnatcasecmp(substr($url, 0, 2), './') != 0) {
      $url = 'http://' . $url;
   }
   return $url;
}

//获取客户端IP地址
function GetClientAddr()
{
   if (!empty($_SERVER['HTTP_CLIENT_IP'])) { //check ip from share internet
      $ip = $_SERVER['HTTP_CLIENT_IP'];
   } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) { //to check ip is pass from proxy
      $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
   } else {
      $ip = $_SERVER['REMOTE_ADDR'];
   }
   return $ip;
}

//获取指定IP地址所在国家
function GetCurrentChileNationur($settings, $addr)
{
   $nationurName = '';
   if (!empty($settings->ipInterAddr)) {
      if ($addr == '127.0.0.1' || $addr == '::1') {
         $nationurName = '本地1';
         return $nationurName;
      }
      $geturl = PerfectUrl($settings->ipInterAddr);
      if (!empty($geturl)) {
         $geturl = $geturl . "?ip=$addr";
         $contents = file_get_contents($geturl);
         $result = json_decode($contents);
         if ($result->success) {
            $location = json_decode($result->value);
            if (isset($location->country)) $nationurName = $location->country;
         } else {
            throw new Exception($result->message);
         }
      }
   } else {
      throw new Exception('没有配置Ip识别的接口地址');
   }
   return $nationurName;
}

//获取配置的国家 跳转的链接数量
function GetNationurlCount($settings)
{
   $count = 0;
   for ($i = 0; $i < sizeof($settings->nationurl); $i++) {
      if (!empty($settings->nationurl[$i]->name) && !empty($settings->nationurl[$i]->url)) $count++;
   }
   return $count;
}

//获取指定国家 要跳转的链接
function GetNationurlJump($settings, $nationurName, $defurl)
{
   if (empty($nationurName)) return $defurl;

   $url = '';
   for ($i = 0; $i < sizeof($settings->nationurl); $i++) {
      if ($settings->nationurl[$i]->name == $nationurName) {
         $url = $settings->nationurl[$i]->url;
         break;
      }
   }
   if (empty($url)) return $defurl;
   return $url;
}

//获取所有国家列表 要跳转的Url
function GetRandomNationurlJump($settings, $accessConut)
{
   $dist = array();
   //如果启用策略跳转  或  (启用计次跳转 且  访问计数跳转>0)
   if ($settings->enablejumpMode == '1' || ($settings->accessCount > 0 && $accessConut > $settings->accessCount)) {
      //配置了国家
      if (isset($settings->nationurl)) {
         for ($i = 0; $i < sizeof($settings->nationurl); $i++) {
            if (!empty($settings->nationurl[$i]->name) && !empty($settings->nationurl[$i]->url)) {
               $url = $settings->nationurl[$i]->url;
               //获取()中间的url
               $url = GetStrMiddle($url, '(', ')');
               $url = PerfectUrl($url);
               $name = array_key_exists($url, $dist) ? $dist[$url] : '';
               $dist[$url] = $name . $settings->nationurl[$i]->name . ' ';
            }
         }
      }
   }
   return $dist;
}

//获取要跳转的url
function GetJumpUrl($settings, $accessConut, $nationurName = null)
{
   //如果启用策略跳转  或  (启用计次跳转 且  访问计数跳转>0)
   if ($settings->enablejumpMode == '1' || ($settings->accessCount > 0 && $accessConut > $settings->accessCount)) {
      //配置了国家
      if (isset($settings->nationurl) && GetNationurlCount($settings) > 0) {
         if ($nationurName == null) $nationurName = GetCurrentChileNationur($settings, GetClientAddr());
         $url = GetNationurlJump($settings, $nationurName, $settings->afterUrl);
      } else {
         $url = $settings->afterUrl;
      }
   } else $url = $settings->priorUrl;
   if (empty($url)) throw new Exception('没有配置要跳转的链接');

   //获取()中间的url
   $url = GetStrMiddle($url, '(', ')');
   $url = PerfectUrl($url);
   return $url;
}
